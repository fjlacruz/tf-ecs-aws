#!/bin/bash
#Author: Francisco Javier La Cruz

echo "
  ___   ___    ___     _               _            _   _             
 |_ _| |   \  | _ \   (_)  _ _    ___ | |_   __ _  | | | |  ___   _ _ 
  | |  | |) | |  _/   | | | ' \  (_-< |  _| / _  | | | | | / -_) | '_|
 |___| |___/  |_|     |_| |_||_| /__/  \__| \__,_| |_| |_| \___| |_|  
"
echo ---------- version de cli aws ------------------

if aws --version = 'command not found'
then
   check="\u221A";
   mensaje='tiene aws cli instalado '
   echo  -e "$mensaje\e[0;32m$check\e[0m\t"
else
   echo 'no tiene aws cli instalado' 
   exit 1
fi

URI_REPO='149200729744.dkr.ecr.us-east-1.amazonaws.com'
repository_name='repo-ecs'


 echo "---------- Ejecutando Login ------------------"
 aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin $URI_REPO

 echo "--- Calculando el Nro de Version de la imagen ---"
#version=$(aws ecr describe-images --repository-name repo-ecs --query 'length(imageDetails)')
version=$(shuf -i 10000000-99999999 -n 1)

 echo "'La version de la nueva imagen es:  $version'"

 echo "---------- Construyendo imagen ----------------"
 docker build -t $repository_name .

 echo "---------- Etiquetando imagen ------------------"
 docker tag repo-ecs:latest $URI_REPO"/"$repository_name":"$version

 echo "---------- Subiendo Imagen --------------------"
 docker push $URI_REPO"/"$repository_name":"$version

 echo "---- Creando nueva revision de tarea ----------"
 aws ecs register-task-definition --cli-input-json file://task-ALB-revision3.json --family task-1 --container-definitions '[{"name": "NEST-1","image":"'$URI_REPO/$repository_name:$version'","cpu":1024,"memory":248,"essential":true,"portMappings":[{"containerPort":3000,"hostPort":3000,"protocol": "tcp"}]}]'


echo "---- Actualizando Servicio ----------"
lastVerion=$(aws ecs describe-task-definition --task-definition task-1 --query 'taskDefinition.revision')
echo "'ejecutando: aws ecs update-service --cluster ELB-ECS-1 --service services-1 --task-definition task-1:$lastVerion'"
aws ecs update-service --cluster ELB-ECS-1 --service services-1 --task-definition task-1:$lastVerion




