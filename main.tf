#=========================================================================================#
#======================= 1. Proceso de creación de la VPC ================================#
#=========================================================================================#
# Crear la VPC
resource "aws_vpc" "VPC-TF" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "VPC-TF"
  }
}
# Crear las subnets públicas
resource "aws_subnet" "public_subnet_1" {
  vpc_id                  = aws_vpc.VPC-TF.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "public-subnet-1"
  }
}
resource "aws_subnet" "public_subnet_2" {
  vpc_id                  = aws_vpc.VPC-TF.id
  cidr_block              = "10.0.2.0/24"
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "public-subnet-2"
  }
}
resource "aws_subnet" "public_subnet_3" {
  vpc_id                  = aws_vpc.VPC-TF.id
  cidr_block              = "10.0.3.0/24"
  availability_zone       = "us-east-1c"
  map_public_ip_on_launch = true

  tags = {
    Name = "public-subnet-3"
  }
}
# Crear el Internet Gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.VPC-TF.id

  tags = {
    Name = "TF-igw"
  }
}
# Configurar las tablas de rutas
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.VPC-TF.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = "public-route-table"
  }
}
# Asociar las subnets a las tablas de rutas correspondientes
resource "aws_route_table_association" "public_subnet_association_1" {
  subnet_id      = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.public_route_table.id
}
resource "aws_route_table_association" "public_subnet_association_2" {
  subnet_id      = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.public_route_table.id
}
resource "aws_route_table_association" "public_subnet_association_3" {
  subnet_id      = aws_subnet.public_subnet_3.id
  route_table_id = aws_route_table.public_route_table.id
}
#=========================================================================================#
#====================== 2. Proceso de creación del Repositorio de ECR ====================#
#=========================================================================================#
resource "aws_ecr_repository" "repo-ecs-tf" {
  name                 = "repo-ecs-tf"
  image_tag_mutability = "IMMUTABLE"
  image_scanning_configuration {
    scan_on_push = true
  }

  tags = {
    Name = "repo-ecs-tf"
  }
}
output "ecr_repository_url" {
  value = aws_ecr_repository.repo-ecs-tf.repository_url
}
#=========================================================================================#
#====================== 3. Proceso de creación del Cluster de ECS ========================#
#=========================================================================================#
resource "aws_ecs_cluster" "cluster-tf" {
  name = "cluster-tf"

}

resource "aws_ecs_cluster_capacity_providers" "cluster-tf" {
  cluster_name = aws_ecs_cluster.cluster-tf.name

  capacity_providers = ["FARGATE", "FARGATE_SPOT"]

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = "FARGATE"
  }
}
#https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_cluster_capacity_providers
#=========================================================================================#
#=================== 4.  Proceso de creación de la task definitions ======================#
#=========================================================================================#
resource "aws_iam_role" "ecs_task_role" {
  name = "ecs-task-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ecs-tasks.amazonaws.com",
        },
      },
    ],
  })
}
resource "aws_ecs_task_definition" "task-ecs-tf" {
  family                   = "task-ecs-tf"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = "256" # Ajusta según tus necesidades
  memory                   = "512" # Ajusta según tus necesidades
  execution_role_arn       = "arn:aws:iam::${var.id_aws_acount}:role/ecsTaskExecutionRole"
  task_role_arn            = aws_iam_role.ecs_task_role.arn

  container_definitions = jsonencode([
    {
      name  = "containerTF",
      image = "${var.id_aws_acount}.dkr.ecr.us-east-1.amazonaws.com/repo-ecs:38889699",
      portMappings = [
        {
          name          = "containertf-3000-tcp",
          containerPort = 3000,
          hostPort      = 3000,
          protocol      = "tcp",
          appProtocol   = "http"
        },
      ]
    },
  ])
}
#=========================================================================================#
#=================== 5.  Proceso de creación del Grupo de Seguridad ======================#
#=========================================================================================#
resource "aws_security_group" "sg_ecs_tf" {
  name        = "sg_ecs_tf"
  description = "Grupo de seguridad para ECS"
  vpc_id      = aws_vpc.VPC-TF.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Permite acceso desde cualquier dirección IP en Internet
  }

  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Permite acceso desde cualquier dirección IP en Internet
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "sg_ecs_tf"
  }
}
#=========================================================================================#
#=================== 6.  Proceso de creación del Target group ============================#
#=========================================================================================#
resource "aws_lb_target_group" "ecs_target_group" {
  name        = "ecs-target-group"
  port        = 80 # Ajusta el puerto según tus necesidades
  protocol    = "HTTP"
  vpc_id      = aws_vpc.VPC-TF.id # Ajusta el ID de tu VPC
  target_type = "ip"              # Puedes ajustar según tu configuración

  health_check {
    path     = "/"
    protocol = "HTTP"
    port     = 80
    matcher  = "200-399"
  }
}
#=========================================================================================#
#=================== 7.  Proceso de creación del Balanceador de carga ====================#
#=========================================================================================#
resource "aws_lb" "ecs_load_balancer" {
  name               = "ecs-load-balancer"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.sg_ecs_tf.id]

  enable_deletion_protection = false

  subnets = [aws_subnet.public_subnet_1.id, aws_subnet.public_subnet_2.id, aws_subnet.public_subnet_3.id]
}
# Asociar el grupo de destinos al balanceador de carga
resource "aws_lb_listener" "ecs_listener" {
  load_balancer_arn = aws_lb.ecs_load_balancer.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ecs_target_group.arn
  }
}
# Salida del DNS del balanceador de carga
output "load_balancer_dns" {
  value = aws_lb.ecs_load_balancer.dns_name
}
#=========================================================================================#
#=================== 8. Proceso de creación del servicio de ECS ==========================#
#=========================================================================================#
resource "aws_ecs_service" "ecs_service_tf" {
  name             = "servicio-ecs-tf"
  cluster          = aws_ecs_cluster.cluster-tf.id
  task_definition  = aws_ecs_task_definition.task-ecs-tf.arn
  launch_type      = "FARGATE"
  desired_count    = 2        # Número deseado de tareas
  platform_version = "LATEST" # Versión de plataforma más reciente

  deployment_controller {
    type = "ECS"
  }
  network_configuration {
    subnets          = [aws_subnet.public_subnet_1.id, aws_subnet.public_subnet_2.id, aws_subnet.public_subnet_3.id] # Ajusta los IDs de las subnets
    security_groups  = [aws_security_group.sg_ecs_tf.id]
    assign_public_ip = true
  }
  # Balanceador de carga
  load_balancer {
    target_group_arn = aws_lb_target_group.ecs_target_group.arn
    container_name   = "containerTF"
    container_port   = 3000
  }
  enable_ecs_managed_tags = true
  enable_execute_command  = true
}
#=========================================================================================#
#=================== 9. Proceso de creación del Auto Escaling ============================#
#=========================================================================================#
resource "aws_appautoscaling_target" "as_target" {
  max_capacity       = 4
  min_capacity       = 2
  resource_id        = "service/${aws_ecs_cluster.cluster-tf.name}/${aws_ecs_service.ecs_service_tf.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "my_policy" {
  name               = "ecs-ae-policy"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.as_target.resource_id
  scalable_dimension = aws_appautoscaling_target.as_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.as_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
    target_value = 50.0
  }
}

